package com.tl.mongo

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import spock.lang.Specification

@SpringBootTest(classes = MongoDemoApp.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class IntegrationTestBaseSpec extends Specification {

    static TestRestTemplate restTemplate

    @Autowired
    void setupRestTemplate(TestRestTemplate testRestTemplate) {
        restTemplate = testRestTemplate
    }

    @LocalServerPort
    private int port

    def createURLWithPort(String uri) {
        return "http://localhost:$port$uri".toString()
    }
}
