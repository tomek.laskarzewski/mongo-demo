package com.tl.mongo.model;

import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "books")
@CompoundIndex(name = "title_authors", def = "{name: 1, authors: 1}", unique = true)
public class Book {

  @Id
  private ObjectId id;
  private String name;
  private List<String> authors;
  private String publisher;
  private int publishedYear;
  private String language;

  public Book(ObjectId id, String name, List<String> authors,
      String publisher, int publishedYear, String language) {
    this.id = id;
    this.name = name;
    this.authors = authors;
    this.publisher = publisher;
    this.publishedYear = publishedYear;
    this.language = language;
  }

  public ObjectId getId() {
    return id;
  }

  public void setId(ObjectId id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<String> getAuthors() {
    return authors;
  }

  public void setAuthors(List<String> authors) {
    this.authors = authors;
  }

  public String getPublisher() {
    return publisher;
  }

  public void setPublisher(String publisher) {
    this.publisher = publisher;
  }

  public int getPublishedYear() {
    return publishedYear;
  }

  public void setPublishedYear(int publishedYear) {
    this.publishedYear = publishedYear;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }
}
