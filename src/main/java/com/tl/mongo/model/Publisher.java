package com.tl.mongo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "publishers")
public class Publisher {

  @Id
  private String name;
  private Integer founded;
  private String location;

  public Publisher(String name, Integer founded, String location) {
    this.name = name;
    this.founded = founded;
    this.location = location;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getFounded() {
    return founded;
  }

  public void setFounded(Integer founded) {
    this.founded = founded;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }
}
