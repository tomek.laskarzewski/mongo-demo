package com.tl.mongo.component;

import com.tl.mongo.model.Publisher;
import java.util.Collection;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PublisherRepository extends MongoRepository<Publisher, String> {
  List<Publisher> findByNameIn(Collection<String> name);
}
