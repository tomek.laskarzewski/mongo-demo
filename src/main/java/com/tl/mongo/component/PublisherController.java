package com.tl.mongo.component;

import com.tl.mongo.model.Publisher;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/publishers")
public class PublisherController {

  private final PublisherService service;

  public PublisherController(PublisherService service) {
    this.service = service;
  }

  @GetMapping("")
  public List<Publisher> find() {
    return service.find();
  }

  @GetMapping("/{name}")
  public Publisher get(@PathVariable String name) {
    return service.get(name);
  }

  @PostMapping("")
  public Publisher save(@RequestBody Publisher publisher) {
    return service.save(publisher);
  }

  @PutMapping("")
  public Publisher update(@RequestBody Publisher publisher) {
    return service.update(publisher);
  }
}
