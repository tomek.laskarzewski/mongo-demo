package com.tl.mongo.component;

import com.tl.mongo.model.Book;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BookRepository extends MongoRepository<Book, ObjectId> {
  List<Book> findBooksByAuthorsContaining(List<String> authors);
}
