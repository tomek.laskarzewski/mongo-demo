package com.tl.mongo.component;

import com.tl.mongo.model.Book;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/books")
public class BookController {

  private final BookService bookService;

  public BookController(BookService bookService) {
    this.bookService = bookService;
  }

  @GetMapping("")
  public List<Book> find(@RequestParam(value = "author", required = false) List<String> authors) {
    return bookService.find(authors);
  }

  @GetMapping("/{id}")
  public Book get(@PathVariable ObjectId id) {
    return bookService.get(id);
  }

  @PostMapping("")
  public Book save(@RequestBody Book book) {
    return bookService.save(book);
  }
}
