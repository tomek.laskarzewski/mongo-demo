package com.tl.mongo.component;

import com.tl.mongo.component.exception.BookNotFoundException;
import com.tl.mongo.model.Book;
import java.util.List;
import java.util.Objects;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

@Service
public class BookService {

  private final BookRepository repository;
  private final AuthorService authorService;
  private final PublisherService publisherService;

  public BookService(BookRepository repository,
      AuthorService authorService, PublisherService publisherService) {
    this.repository = repository;
    this.authorService = authorService;
    this.publisherService = publisherService;
  }

  public List<Book> find(List<String> authors) {
    return (Objects.isNull(authors) || authors.isEmpty())
        ? repository.findAll()
        : repository.findBooksByAuthorsContaining(authors);
  }

  public Book get(ObjectId id) {
    return repository.findById(id).orElseThrow(() -> new BookNotFoundException(id.toString()));
  }

  public Book save(Book book) {
    authorService.existsAll(book.getAuthors());
    publisherService.exists(book.getPublisher());
    return repository.save(book);
  }
}
