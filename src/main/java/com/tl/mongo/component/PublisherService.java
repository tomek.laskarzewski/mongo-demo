package com.tl.mongo.component;

import com.tl.mongo.component.exception.PublisherExistsException;
import com.tl.mongo.component.exception.PublisherNotFoundException;
import com.tl.mongo.model.Publisher;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class PublisherService {

  private final PublisherRepository repository;

  public PublisherService(PublisherRepository repository) {
    this.repository = repository;
  }

  public Publisher get(String name) {
    return repository.findById(name).orElseThrow(() -> new PublisherNotFoundException(name));
  }

  public List<Publisher> find() {
    return repository.findAll();
  }

  public Publisher save(Publisher publisher) {
    if (exists(publisher.getName())) {
      throw new PublisherExistsException(publisher.getName());
    }
    return repository.save(publisher);
  }

  public Publisher update(Publisher publisher) {
    if (!exists(publisher.getName())) {
      throw new PublisherNotFoundException(publisher.getName());
    }
    return repository.save(publisher);
  }

  public boolean exists(String name) {
    return repository.existsById(name);
  }
}
