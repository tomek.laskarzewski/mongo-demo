package com.tl.mongo.component;

import com.tl.mongo.component.exception.AuthorNotFoundException;
import com.tl.mongo.component.exception.PublisherExistsException;
import com.tl.mongo.component.exception.PublisherNotFoundException;
import com.tl.mongo.model.Author;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class AuthorService {

  private final AuthorRepository repository;

  public AuthorService(AuthorRepository repository) {
    this.repository = repository;
  }

  public List<Author> find() {
    return repository.findAll();
  }

  public Author get(String name) {
    return repository.findById(name).orElseThrow(() -> new AuthorNotFoundException(name));
  }

  public boolean existsAll(List<String> authors) {
    authors.forEach(author -> {
        if (!repository.existsById(author)) {
          throw new AuthorNotFoundException(author);
        }
    });
    return true;
  }

  public Author save(Author author) {
    if (repository.existsById(author.getName())) {
      throw new PublisherExistsException(author.getName());
    }
    return repository.save(author);
  }

  public Author update(Author author) {
    if (!repository.existsById(author.getName())) {
      throw new PublisherNotFoundException(author.getName());
    }
    return repository.save(author);
  }

}
