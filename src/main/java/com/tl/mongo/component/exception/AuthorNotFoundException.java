package com.tl.mongo.component.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class AuthorNotFoundException extends ResponseStatusException {
  public AuthorNotFoundException(String name) {
    super(
        HttpStatus.NOT_FOUND,
        String.format("Author not found %s.", name));
  }
}
