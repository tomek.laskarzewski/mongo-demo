package com.tl.mongo.component.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class AuthorExistsException extends ResponseStatusException {
  public AuthorExistsException(String name) {
    super(
        HttpStatus.BAD_REQUEST,
        String.format("Author exists %s.", name));
  }
}
