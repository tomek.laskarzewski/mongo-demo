package com.tl.mongo.component.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class PublisherNotFoundException extends ResponseStatusException {
  public PublisherNotFoundException(String id) {
    super(
        HttpStatus.NOT_FOUND,
        String.format("Publisher not found %s.", id));
  }
}
