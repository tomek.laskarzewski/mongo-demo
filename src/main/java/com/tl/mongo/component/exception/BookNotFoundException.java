package com.tl.mongo.component.exception;

import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class BookNotFoundException extends ResponseStatusException {
  public BookNotFoundException(String id) {
    super(
        HttpStatus.NOT_FOUND,
        String.format("Book not found %s.", id));
  }
}
