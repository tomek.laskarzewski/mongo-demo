package com.tl.mongo.component.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class PublisherExistsException extends ResponseStatusException {
  public PublisherExistsException(String id) {
    super(
        HttpStatus.BAD_REQUEST,
        String.format("Publisher exists %s.", id));
  }
}
