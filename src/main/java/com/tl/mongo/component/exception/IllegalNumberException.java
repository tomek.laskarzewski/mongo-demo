package com.tl.mongo.component.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class IllegalNumberException extends ResponseStatusException {
  public IllegalNumberException() {
    super(
        HttpStatus.BAD_REQUEST,
        String.format("Number must be positive integer."));
  }
}
