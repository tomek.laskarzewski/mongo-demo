package com.tl.mongo.component.numbers;

import com.tl.mongo.component.exception.IllegalNumberException;

public class FibonacciService {

  private final NumbersService numbersService;

  public FibonacciService(NumbersService numbersService) {
    this.numbersService = numbersService;
  }

  public long[] getFibonacci(int count) {
    if (count < 0) {
      throw new IllegalNumberException();
    }
    long[] numbers = new long[count];
    for (int i = 0; i < count; i++) {
      numbers[i] = i < 2
          ? numbersService.add(0, i)
          : numbersService.add(numbers[i-2], numbers[i-1]);
    }
    return numbers;
  }

}
