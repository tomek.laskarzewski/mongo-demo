package com.tl.mongo.component.numbers;

public class NumbersService {

  public long add(long a, long b) {
    return a + b;
  }

  public long substract(long a, long b) {
    return a - b;
  }

  public long multiply(long a, long b) {
    return a * b;
  }

  public double divide(long a, long b) {
    return a / (b * 1.);
  }
}
