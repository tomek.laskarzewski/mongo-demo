package com.tl.mongo.component;

import com.tl.mongo.model.Author;
import java.util.Collection;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AuthorRepository extends MongoRepository<Author, String> {
  List<Author> findAllByNameIn(Collection<String> id);
}
