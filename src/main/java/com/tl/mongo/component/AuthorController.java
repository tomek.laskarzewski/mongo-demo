package com.tl.mongo.component;

import com.tl.mongo.model.Author;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authors")
public class AuthorController {

  private final AuthorService service;

  public AuthorController(AuthorService service) {
    this.service = service;
  }

  @GetMapping("")
  public List<Author> find() {
    return service.find();
  }

  @GetMapping("/{name}")
  public Author get(@PathVariable String name) {
    return service.get(name);
  }

  @PostMapping("")
  public Author save(@RequestBody Author author) {
    return service.save(author);
  }

  @PutMapping("")
  public Author update(@RequestBody Author author) {
    return service.update(author);
  }
}
