package com.tl.mongo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@Slf4j
public class MongoDemoApp {

  public static void main(String... args) {
    log.info("Started rest-service...");
    SpringApplication.run(MongoDemoApp.class, args);
  }
}
